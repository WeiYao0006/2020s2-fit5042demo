package helloworld;

public class NameStorageBean {
	private String name = "World";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}